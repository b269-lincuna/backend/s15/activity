console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

	let myFirstName = "John";
	let myLastName = "Smith";
	let myAge = 30;
	let myHobbies = ["Biking","Mountian Climbing","Swimming"];
	let myWorkLocation = {
			houseNumber: "32",
			street: "Washington",
			city: "Lincoln",
			state: "Nebraska"
		}

	console.log("First Name: " + myFirstName);
	console.log("Last Name: " + myLastName);
	console.log("Age: " + myAge);
	console.log("Hobbies:");
	console.log(myHobbies);
	console.log("Work Address: ");
	console.log(myWorkLocation);


/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName); //input proper variable name from [name] to [fullName]

	let age = 40;
	console.log("My current age is: " + age); //input proper variable name from [currentAge] to [age]
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"]; //input proper quotation and comma
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {
		userName: "captain_america",
		pFullName: "Steve Rogers",
		age: 40,
		isActive: false,
	}

	/* [fullName] variable inside profile object does not affected from above declared variable
	    but for best practice, i change it to diffent name from [fullName] to [pFullName]
	*/

	console.log("My Full Profile: ")
	console.log(profile);

	let bestFriendFullName = "Bucky Barnes";
	//change variable name to prevent duplication and change from [fullName] to [bestFriendFullName] 
	console.log("My bestfriend is: " + bestFriendFullName);

	const lastLocation = "Arctic Ocean";
	// lastLocation = "Atlantic Ocean";  // cannot change the value of constant variables
	console.log("I was found frozen in: " + lastLocation);

